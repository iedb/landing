---
layout: documentation
title: Privacy notice

namespace: privacy
permalink: /privacy
permalink_en: /privacy

languages: ["en"]
---

### Cookies and analytics

- This application does not use cookies except when strictly necessary (authentication).
- This website does not store nominative data on users.
- This website collects anonymous usage metrics. Thoses metrics are not shared outsite of the OgmAPI staff.

### Account management

- All data relating to an account can be accessed via the account view.
- By deleting an account, you delete all the account's data.
- Unverified (email) accounts older than 6 months will be removed.
- Service emails are sent through [Brevo](https://www.brevo.com).

### Feedback

- The feedback feature collects email as a unique identifier, and as a means of contact related to the feedback.
- This email is not used for any other purpose.
- You can access and remove your information using the contact address below.

### Contact

For any question you can contact us at contact@ogmapi.fr