---
layout: documentation
title: Déclaration de confidentialité

namespace: privacy
permalink: /privacy

languages: ["fr"]
---

### Cookies et statistiques d'usage

- Cette application n'utilise pas de cookies en dehors de cas strictement nécessaires (authentification).
- Ce site ne stocke pas de données nominatives sur les utilisateurs.
- Ce site web recueille des mesures d'utilisation anonymes. Ces mesures ne sont pas partagées en dehors de l'équipe de l'OgmAPI.

### Gestion de comptes

- Toutes les données relatives à votre compte sont accessibles via la vue du compte.
- En supprimant votre compte, vous supprimez toutes les données relatives à votre compte.
- Les comptes non vérifiés (email) datant de plus de 6 mois seront supprimés.
- Les emails de service sont envoyés via [Brevo](https://www.brevo.com).

### Feedback

- La fonction de feedback recueille l'adresse électronique en tant qu'identifiant et en tant que moyen de contact suite à votre feedback.
- Cette adresse électronique n'est pas utilisée à d'autres fins.
- Vous pouvez accéder à vos informations et les supprimer en utilisant l'adresse de contact ci-dessous.

### Contact

Pour toutes question, vous pouvez nous contacter à l'adresse suivante: contact@ogmapi.fr