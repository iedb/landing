---
layout: documentation
title: Jeux de données

namespace: datasets
permalink: /datasets

languages: ["fr"]
---

Cette page contient un inventaire (classé par ordre alphabétique) des jeux de données actuellement disponibles sur OgmAPI.
Notez que certains jeux de données ne contenant pas directement d'information géographique sont intégrés dans des jeux de données parents portant l'information géographique.
Veillez vous référer à la [page de documentation](https://docs.ogmapi.fr/) associée pour plus d'informations sur leur intégration dans notre base de données.

Notre service est encore en phase de développement et toutes les données de data.gouv.fr n'ont pas encore été importées.
N'hésitez pas à [nous contacter]({{ 'mailto:' |append: site.email }}) si vous avez un jeu de données particulier en tête.
Nous l'ajouterons dès que possible.

## [Adresse et géolocalisation des établissements d'enseignement du premier et second degrés](https://www.data.gouv.fr/fr/datasets/adresse-et-geolocalisation-des-etablissements-denseignement-du-premier-et-second-degres-1/)

Liste géolocalisée des établissements d'enseignement des premier et second degrés (tous ministères de tutelle, secteurs public et privé) situés en France.

## [Carte des loyers](https://www.data.gouv.fr/fr/datasets/carte-des-loyers-indicateurs-de-loyers-dannonce-par-commune-en-2022/)

Indicateurs de loyers d'annonce par commune et arrondissement.

## [Compétence territoriale gendarmerie et police nationales](https://www.data.gouv.fr/fr/datasets/competence-territoriale-gendarmerie-et-police-nationales/)

Ce fichier d'export définit les zones de compétence de la gendarmerie et de la police nationales.

## [IGN Admin Express](https://geoservices.ign.fr/adminexpress)

Le jeu de données IGN Admin Express contient les limites administratives de la France métropolitaine et des territoires d'outre-mer.

## [Immeubles protégés au titre des Monuments Historiques](https://www.data.gouv.fr/fr/datasets/immeubles-proteges-au-titre-des-monuments-historiques-2)

Liste des édifices et immeubles qui ont été ou sont protégés au titre des Monuments historiques (classés ou inscrits), depuis l’année 1840 jusqu’à aujourd’hui.

## [Liste des communes selon le zonage ABC](https://www.data.gouv.fr/fr/datasets/liste-des-communes-selon-le-zonage-abc/)

Le zonage conventionnellement appelé ABC effectue un « classement des communes du territoire national en zones géographiques en fonction du déséquilibre entre l'offre et de la demande de logements ».


## [ Zones de défense et de sécurité 2016](https://www.data.gouv.fr/fr/datasets/zones-de-defense-et-de-securite-2016/)

