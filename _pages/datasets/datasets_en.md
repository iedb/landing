---
layout: documentation
title: Datasets

namespace: datasets
permalink: /datasets
permalink_en: /datasets

languages: ["en"]
---

This page contains an inventory (sorted alphabetically) of the datasets currently available on OgmAPI.
Note that some datasets that do not directly contain geographic information are integrated into parent datasets carrying the geographic information.
See the associated [documentation page](https://docs.ogmapi.fr/) for more information on how they are present in our database.

Our service is still in its development stage and not all data from data.gouv.fr has yet been imported.
Please do not hesitate to [contact us]({{ 'mailto:' |append: site.email }}) if you have a particular dataset in mind.
We'll add it as soon as possible.

## [Adresse et géolocalisation des établissements d'enseignement du premier et second degrés](https://www.data.gouv.fr/fr/datasets/adresse-et-geolocalisation-des-etablissements-denseignement-du-premier-et-second-degres-1/)

Geolocated list of primary and secondary schools (all ministries, public and private sectors) located in France.

## [Carte des loyers](https://www.data.gouv.fr/fr/datasets/carte-des-loyers-indicateurs-de-loyers-dannonce-par-commune-en-2022/)

Advertisement rent indicators by municipality and district.

## [Compétence territoriale gendarmerie et police nationales](https://www.data.gouv.fr/fr/datasets/competence-territoriale-gendarmerie-et-police-nationales/)

This dataset defines the areas of jurisdiction of the national gendarmerie and police.

## [IGN Admin Express](https://geoservices.ign.fr/adminexpress)

The IGN Admin Express dataset contains the administrative boundaries of mainland france and oversees territories.

## [Immeubles protégés au titre des Monuments Historiques](https://www.data.gouv.fr/fr/datasets/immeubles-proteges-au-titre-des-monuments-historiques-2)

List of buildings and properties that have been or are protected as Historic Monuments (classified or listed), from 1840 to the present day.

## [Liste des communes selon le zonage ABC](https://www.data.gouv.fr/fr/datasets/liste-des-communes-selon-le-zonage-abc/)

Conventionally known as ABC zoning, this involves "classifying the country's municipalities into geographical zones according to the imbalance between housing supply and demand".

## [ Zones de défense et de sécurité 2016](https://www.data.gouv.fr/fr/datasets/zones-de-defense-et-de-securite-2016/)


