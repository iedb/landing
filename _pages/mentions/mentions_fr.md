---
layout: documentation
title: Mentions légales

namespace: mentions
permalink: /legal-informations

languages: ["fr"]
---

L'ensemble de ce site relève des législations françaises et internationales sur le droit d'auteur et la propriété intellectuelle. Tous les droits de reproduction sont réservés, y compris pour les documents iconographiques et photographiques.

## OgmAPI

Contact: contact@ogmapi.fr
<br>
Auteur: [Antonin Voyez](https://voyez.eu)

## Citations

- [https://www.data.gouv.fr](https://www.data.gouv.fr)
- [https://www.openstreetmap.org](https://www.openstreetmap.org)
- [https://getbootstrap.com/](https://getbootstrap.com/)
- [https://icons.getbootstrap.com/](https://icons.getbootstrap.com/)
- [https://github.com/lipis/flag-icons](https://github.com/lipis/flag-icons)

## Hébergeur: Pulseheberg

[SAS PulseHeberg](https://pulseheberg.com/)
<br>
9 Boulevard de Strasbourg, 83000 Toulon, France
<br>
Téléphone: +33 (0) 4 22 14 13 60
<br>
Email: contact@pulseheberg.com
