---
layout: documentation
title: Legal informations

namespace: mentions
permalink: /legal-informations

languages: ["en"]
---

This entire site is governed by French and international legislation on copyright and intellectual property. All reproduction rights are reserved, including for iconographic and photographic documents.

## OgmAPI

Contact: contact@ogmapi.fr
<br>
Author: [Antonin Voyez](https://voyez.eu)

## References

- [https://www.data.gouv.fr](https://www.data.gouv.fr)
- [https://www.openstreetmap.org](https://www.openstreetmap.org)
- [https://getbootstrap.com/](https://getbootstrap.com/)
- [https://icons.getbootstrap.com/](https://icons.getbootstrap.com/)
- [https://github.com/lipis/flag-icons](https://github.com/lipis/flag-icons)

## Host: Pulseheberg

[SAS PulseHeberg](https://pulseheberg.com/)
<br>
9 Boulevard de Strasbourg, 83000 Toulon, France
<br>
Téléphone: +33 (0) 4 22 14 13 60
<br>
Email: contact@pulseheberg.com
