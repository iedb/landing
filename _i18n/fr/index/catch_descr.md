OgmAPI est une base de données unifiant plusieurs jeux de données géographiques.
<br>
Le service regroupe 12 jeux de données représentant près de 90 millions d'éléments.
<br>
Les données sont accessibles au travers d'une interface web et d'une API.