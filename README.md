# OgmAPI landing website

This is designed to be the landing site for OgmAPI (former IEDB).
It is currently deployed at [https://ogmapi.fr](https://ogmapi.fr).

## How to use

See [jekyll](https://jekyllrb.com/) for the jekyll setup.

- Build the dependencies
```
bundler install
```

- Launch the dev server
```
bundle exec jekyll serve
```

- Deploy the production version:
```
bundle exec jekyll build